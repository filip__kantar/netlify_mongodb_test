import { HandlerEvent, HandlerContext } from "@netlify/functions";
import { omit } from "lodash";
//import validateRequest from "../../../src/middleware/valdiateRequest";
import log from "../../../src/logger";
import { AnySchema } from "yup";
import { DocumentDefinition } from "mongoose";
import mongodb, { Db } from "mongodb";
import config from "../../../config/default";

/*export const handler = async (event: HandlerEvent) => {
  try {
    const type = event?.queryStringParameters?.type || "pizza";
    const user = await createUser(req.body);
    return res.send(omit(user.toJSON(), "password"));
  } catch (e: any) {
    log.error(e);
    return res.status(409).send(e.message);
  }
};*/
const MongoClient = mongodb.MongoClient;
let cachedDb: Db = null;
const MONGODB_URI: string = config.dbUri;
const DB_NAME: string = config.dbName;

const connectToDatabase = async (uri) => {
  // we can cache the access to our database to speed things up a bit
  // (this is the only thing that is safe to cache here)
  if (cachedDb) return cachedDb;

  const client: mongodb.MongoClient = await MongoClient.connect(uri);

  cachedDb = client.db(DB_NAME);

  return cachedDb;
};
const pushToDatabase = async (db: Db, data: mongodb.Document) => {
  console.log(data.name);
  console.log(data.email);
  if (data.name && data.email) {
    await db.collection("users").insertMany([data]);
    return { statusCode: 201 };
  } else {
    return { statusCode: 422 };
  }
};
const updateDatabase = async (db: Db, data: mongodb.Document) => {
  if (data.name && data.email) {
    await db
      .collection("users")
      .updateMany({ email: data.email }, { $set: { password: data.password } });
    return { statusCode: 201 };
  } else {
    return { statusCode: 422 };
  }
};
const queryDatabase = async (db: Db) => {
  const allUsers: mongodb.Document[] = await db
    .collection("users")
    .find({})
    .toArray();

  return {
    statusCode: 200,
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(allUsers),
  };
};
export const handler = async (event: HandlerEvent, context: HandlerContext) => {
  context.callbackWaitsForEmptyEventLoop = false;

  const db: Db = await connectToDatabase(MONGODB_URI);

  switch (event.httpMethod) {
    case "GET":
      return queryDatabase(db);
    case "POST":
      return pushToDatabase(db, JSON.parse(event.body));
    case "PUT":
      return updateDatabase(db, JSON.parse(event.body));
    default:
      return { statusCode: 400 };
  }
};
