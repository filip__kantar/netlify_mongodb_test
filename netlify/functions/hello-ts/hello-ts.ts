import { HandlerEvent } from "@netlify/functions";

type Pizza = {
  sauce: "red" | "white";
  type: "pizza";
};
type HotDog = {
  size: "ball park" | "footlong";
  type: "hotdog";
};
type Taco = {
  spiceLevel: "none" | "mild" | "hot";
  type: "taco";
};

type Sandwich = Pizza | HotDog | Taco;

function serveSandwich(sandwich: Sandwich): string {
  switch (sandwich.type) {
    case "pizza":
      return `Pizza with ${sandwich.sauce} sauce`;
    case "hotdog":
      return `Hotdog with ${sandwich.size} size`;
    case "taco":
      return `Taco with ${sandwich.spiceLevel} spice`;
    default:
      break;
  }
}

export const handler = async (event: HandlerEvent) => {
  try {
    const type = event?.queryStringParameters?.type || "pizza";

    const pizza: Pizza = {
      sauce: "red",
      type: "pizza",
    };
    const hotdog: HotDog = {
      size: "footlong",
      type: "hotdog",
    };
    const taco: Taco = {
      spiceLevel: "hot",
      type: "taco",
    };

    const sandwich =
      type === "pizza" ? pizza : type === "hotdog" ? hotdog : taco;
    return {
      statusCode: 200,
      body: JSON.stringify({ message: serveSandwich(sandwich) }),
    };
  } catch (error) {
    return { statusCode: 500, body: error.toString() };
  }
};
