import mongoose, { mongo } from "mongoose";
import config from "../../../config/default";
import log from "../../../src/logger";
import { HandlerEvent } from "@netlify/functions";

var dbInfoMessage: String = "";

function connect() {
  const dbUri = config.dbUri as string;
  return mongoose
    .connect(dbUri)
    .then(() => {
      log.info("Databse Connected");
      dbInfoMessage = "Database connected";
    })
    .catch((error) => {
      log.error("db error", error);
      dbInfoMessage = `db error ${error}`;
      process.exit(1);
    });
}
export const handler = async (event: HandlerEvent) => {
  try {
    await connect();
    return {
      statusCode: 200,
      body: JSON.stringify({
        message: `Welcome to the app, Database status: ${dbInfoMessage}`,
      }),
    };
  } catch (error) {
    return { statusCode: 500, body: error.toString() };
  }
};
