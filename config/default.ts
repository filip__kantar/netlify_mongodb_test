export default {
  port: 1337,
  host: "localhost",
  dbUri: "mongodb+srv://admin:admin@freecluster.rcl8m.mongodb.net",
  dbName: "test",
  saltWorkFactor: 10,
  refreshhTokenTlt: "15m",
  accessTokenTlt: "1y",
  privateKey:
    "-----BEGIN RSA PRIVATE KEY-----\r\nMIICXAIBAAKBgQCEbpMuSMcWhwGPAINiPJcGmH/MlaRFXADNQzJJQxzt/w8AzYiD\
      r\nYOyJO3BRIL25iy519tq9xfBNQau4r5ezYq1VQVk5yZd4kg0Q0UXZV7vNg+PHDKqw\r\nwn7J/BPLUWavCtqynHp5fphICewm7ngtJe/KJig8ztEdYKfZwP7942Zq3wIDAQAB\
      r\nAoGAIAvt+85Ktw3Wd7dlE7mbxlHS4zv2833PB9zR516hOop1I/mYpkonExJaHJey\r\nsZqXlJ420qN6ggd5OYjsSdGAJfssQfjPWOfai0bgAQH2Udu6zDsTufyNqs+L6z4V\r\
      niK5iMd0yDy507yRpyR836IG3TAxYxj9APYLJqqWMQpk5JkkCQQD4wCU3tVGiPk1W\r\nNsGWH+kZAUs2QBud1Pakqpe2KRaYKE/47bXuw/47cM6yvNp/AznO7gBdxHZ7GlPv\r\
      nY5fY21UdAkEAiEqcSvcgHxji/+/9Et0gQYzI/+LwLlaS9nLj7Z4+MrHdYNXNML/f\r\n9MneOuhx5LQiYqzaR7emksnjQ0hDfSxrKwJAZkGuywjZpyWPazqVOB6kFyhl/8Y4\
      r\n1Bgg0C5ksXiQljo0LfOiT6X4U/0aq3VhKMdtszg5pFJIFnELQiSh3n7DrQJABew4\r\nHoneu3s5YiXruYYigLa9hWE3Tsw0VsFnZkwaJ9nS5QJliFjOQrUDE0xGpsmtNNyO\r\
      nH8+7TWW4mpPpWuczQQJBAJIPl4ZjmUWGyp2FDacdqy1Db2SwXNGtxh2xn5I165oU\r\nZ/YMKGHZJ/N7g+oE3t1o75ANjk9iMIuOSprYYaCnMEs=\r\
      n-----END RSA PRIVATE KEY-----",
};
