var __defProp = Object.defineProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};

// netlify/functions/user-controller2.ts
__export(exports, {
  handler: () => handler
});
var handler = async (event) => {
  var _a;
  try {
    const type = ((_a = event == null ? void 0 : event.queryStringParameters) == null ? void 0 : _a.type) || "pizza";
    const body = (event == null ? void 0 : event.body) || "test body<";
    return {
      statusCode: 200,
      body: JSON.stringify({ message: body })
    };
  } catch (error) {
    return { statusCode: 500, body: error.toString() };
  }
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  handler
});
//# sourceMappingURL=user-controller2.js.map
