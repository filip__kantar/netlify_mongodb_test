var __defProp = Object.defineProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};

// netlify/functions/hello-ts/hello-ts.ts
__export(exports, {
  handler: () => handler
});
function serveSandwich(sandwich) {
  switch (sandwich.type) {
    case "pizza":
      return `Pizza with ${sandwich.sauce} sauce`;
    case "hotdog":
      return `Hotdog with ${sandwich.size} size`;
    case "taco":
      return `Taco with ${sandwich.spiceLevel} spice`;
    default:
      break;
  }
}
var handler = async (event) => {
  var _a;
  try {
    const type = ((_a = event == null ? void 0 : event.queryStringParameters) == null ? void 0 : _a.type) || "pizza";
    const pizza = {
      sauce: "red",
      type: "pizza"
    };
    const hotdog = {
      size: "footlong",
      type: "hotdog"
    };
    const taco = {
      spiceLevel: "hot",
      type: "taco"
    };
    const sandwich = type === "pizza" ? pizza : type === "hotdog" ? hotdog : taco;
    return {
      statusCode: 200,
      body: JSON.stringify({ message: serveSandwich(sandwich) })
    };
  } catch (error) {
    return { statusCode: 500, body: error.toString() };
  }
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  handler
});
//# sourceMappingURL=hello-ts.js.map
